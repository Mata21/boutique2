/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testweb;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author toma
 */
public class Clientweb {
    
    public static void main(String[] args) {

        // TODO code application logic here
        // Création du client http
        Client client = ClientBuilder.newClient();

        // Ciblage de la resource
        WebTarget webtarget = client.target("http://localhost:8080/api");

        //Identification de la resource
        WebTarget resourceWebTarg = webtarget.path("/hello");

        // Invocation de la requête et traitement
        Invocation.Builder invocationBuilder = resourceWebTarg.request(MediaType.TEXT_PLAIN);


        //invocationBuilder.header("some-header", "true");
        Response response = invocationBuilder.get();
        System.out.println("Statut : "+response.getStatus());
    }
    
}
