/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Produit;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitService;

/**
 *
 * @author toma
 */
@Path("/produit")
public class ProduitResource {
    
    ProduitService produitService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Produit produit) {
        produitService.ajouter(produit);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Produit produit) {
        produitService.modifier(produit);
    }
    
    @GET
    @Path("/{produitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Produit trouver(@PathParam("id") Long id) {
        return produitService.trouver(id);
    }
    
    @DELETE
    @Path("/supprId")
    public void supprimer(@QueryParam("id") Long id) {
        produitService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(@QueryParam("produit") Produit produit) {
        produitService.supprimer(produit);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> lister() {
        return produitService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit>lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return produitService.lister(debut, nombre);
    }
    
    
}
