/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 *
 * @author toma
 */
@Path("/hello")
public class HelloResource {
    
    @GET
    public String saluer(@QueryParam("p1") String nom, @QueryParam("p2") String prenom) {
        return "Bonjour "+ nom +" "+prenom+" Hello word Java";
    }
}
