/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

/**
 *
 * @author toma
 */
public class MyExceptionMapper implements ExceptionMapper<Exception>{

    @Override
    public Response toResponse(Exception e) {
        e.printStackTrace();
        return Response
                       .status(Status.INTERNAL_SERVER_ERROR)
                       .type(MediaType.APPLICATION_XML)
                       .entity(e.getCause())
                       .build();
    }
    
    
}
