/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Achat;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import service.AchatService;

/**
 *
 * @author toma
 */
@Path("/achat")
public class AchatResource {
    
    AchatService achatService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Achat achat) {
        achatService.ajouter(achat);
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Achat achat){
        achatService.modifier(achat);
    }
    
    @GET 
    @Path("/{achatId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Achat trouver(@PathParam("id") Long id) {
        return achatService.trouver(id);
    }
    
    @DELETE
    @Path("/{supprId}")
    public void supprimer(@PathParam("id") Long id) {
        achatService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Achat achat) {
        achatService.supprimer(achat);
    }
    
    @GET 
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> lister() {
        return achatService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return achatService.lister(debut, nombre);
    }
    
    
}
