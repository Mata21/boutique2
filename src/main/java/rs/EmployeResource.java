/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Employee;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.EmployeService;

/**
 *
 * @author toma
 */
@Path("/employe")
public class EmployeResource {
    
    EmployeService employeService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Employee employe)  {
        employeService.ajouter(employe);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Employee employe) {
        employeService.modifier(employe);
    }
    
    @GET
    @Path("/{employeId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Employee trouver(@PathParam("id") Long id) {
        return employeService.trouver(id);
    }
    
    @DELETE
    @Path("/{suppId}")
    public void supprimer(@PathParam("id") Long id) {
        employeService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Employee employe) {
        employeService.supprimer(employe);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> lister() {
        return employeService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return employeService.lister(debut, nombre);
    }
    
    
}
