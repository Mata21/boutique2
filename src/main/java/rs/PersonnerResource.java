/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Personne;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.PersonneService;

/**
 *
 * @author toma
 */
@Path("/personne")
public class PersonnerResource {
    
    PersonneService personneService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Personne personne) {
        personneService.ajouter(personne);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Personne personne) {
        personneService.modifier(personne);
    }
    
    @GET
    @Path("/{personneId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Personne trouver(@PathParam("id") Long id) {
        return personneService.trouver(id);
    }
    
    @DELETE
    @Path("/supprId")
    public void supprimer(@QueryParam("id") Long id) {
        personneService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(@QueryParam("personner") Personne personne) {
        personneService.supprimer(personne);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> lister() {
        return personneService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return personneService.lister(debut, nombre);
    }
    
}
