/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.ProduitAchete;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitAcheteService;

/**
 *
 * @author toma
 */
@Path("/produitachete")
public class ProduitAcheteResource {
 
    ProduitAcheteService produitAcheteService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(ProduitAchete produitAchete) {
        produitAcheteService.ajouter(produitAchete);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(ProduitAchete produitAchete) {
        produitAcheteService.modifier(produitAchete);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(ProduitAchete produitAchete) {
        produitAcheteService.supprimer(produitAchete);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> lister() {
        return produitAcheteService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return produitAcheteService.lister(debut, nombre);
    }
}
