/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Client;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ClientService;

/**
 *
 * @author toma
 */
@Path("/client")
public class ClientResource {
    
    ClientService clientService;
    
    @POST
    @Path("/ajouter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Client client) {
        clientService.ajouter(client);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Client client) {
        clientService.modifier(client);
    }
    
    @GET
    @Path("/{clientId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Client trouver(@PathParam("id") Long id) {
        return clientService.trouver(id);
    }
    
    @DELETE
    @Path("/{supprId}")
    public void supprimer(@PathParam("id") Long id) {
        clientService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Client client) {
        clientService.supprimer(client);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Client> lister() {
        return clientService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Client> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return clientService.lister(debut, nombre);
    }
    
}
