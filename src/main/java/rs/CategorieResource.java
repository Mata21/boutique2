/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entite.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;


import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.CategorieService;

/**
 *
 * @author toma
 */
@Path("/categorie")
public class CategorieResource {
    
    CategorieService categorieService = new CategorieService();
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ajouter")
    public void ajouter(Categorie e){
        categorieService.ajouter(e);
    }
    
    @PUT
    @Path("/modifier")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Categorie e) {
        categorieService.modifier(e);
    }
    
    @GET
    @Path("/{categorieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categorie trouver(@QueryParam("id") Integer id){
        return categorieService.trouver(id);
    }
    
    @DELETE
    @Path("/{supprId}")
    public void supprimer(@QueryParam("id") Integer id) {
    //    categorieService.supprimer(id);
    }
    
    @DELETE
    @Path("/supprimer")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Categorie e) {
        categorieService.supprimer(e);
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> lister() {
        return categorieService.lister();
    }
    
    @GET 
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return categorieService.lister(debut, nombre);
    }    
}
