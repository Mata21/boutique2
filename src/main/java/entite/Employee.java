/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author toma
 */
public class Employee extends Personne{
    
    private String cnss;
    
    
    public Employee() {
        
    }
    
    public String getCnss(){ return this.cnss;}
    public void setCnss(String cnss) {this.cnss = cnss;}
    
    // Redéfintition des methodes
    @Override
    public int hashCode(){
        int hash = 1;
        hash = hash * 3 + super.hashCode();
        hash = hash * 6 + this.cnss.hashCode();
      
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {
            return false;
        }
        return true;
    }
    
    public String toString() {
        return super.toString()+"\nCnss : "+this.cnss;
    }
    
}
