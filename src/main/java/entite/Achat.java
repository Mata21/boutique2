/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.time.LocalDate;
import java.util.List;
import java.util.LinkedList;
import java.util.Objects;


/**
 *
 * @author toma
 */
public class Achat {
    

    private long id;
    private double remise;
    private  LocalDate dateAchat;
    private List<ProduitAchete> listProdAchete = new LinkedList<>();
            
    
    public Achat() {}
    
    
    // Les Methodes
    
    public double getRemiseTotale() {
        double total = this.remise;
        for(ProduitAchete produitAchete : this.listProdAchete) {
            total += produitAchete.getRemise();
        }
        return total;
    }
    
    public List<ProduitAchete> getProdAchete() {return  this.listProdAchete;}
    public void setProdAchete( ProduitAchete prodAchat) { this.listProdAchete.add(prodAchat); }
    
    public double getPrixTotal() {
        double prix = -this.remise;
        for(ProduitAchete produitAchete : this.listProdAchete) {
            prix += produitAchete.getPrixTotal();
        }
        return prix < 0 ? 0. : prix;
    }
    
    public LocalDate geDateAchat() {return this.dateAchat;}
    public Long getId() { return this.id;}
    
    // Redéfinition des méthodes
    @Override
    public String toString() {
        return "\nRemise Totale : "+this.remise+" %"+"\nPrix Total : "+this.getPrixTotal()+"\nDate d'Achat : "+this.dateAchat;
    }
    
    public int hashCode() {
        int hash = 1;
        hash = hash * 22 + (int)(hash * 14 + this.remise);
        hash = hash * 20 + this.dateAchat.hashCode();
        hash = hash * 3 + this.listProdAchete.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateAchat, other.dateAchat)) {
            return false;
        }
        if (!Objects.equals(this.listProdAchete, other.listProdAchete)) {
            return false;
        }
        return true;
    }
    
}
