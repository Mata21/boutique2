/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

/**
 *
 * @author toma
 */
public abstract class Entity <ID>{
    
    protected ID id;
    
    public abstract ID getID();
    
    
}
