/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.util.Objects;


/**
 *
 * @author toma
 */
public class Categorie {    
    private Integer id; 
    private String libelle;
    private String description;
    
    
    // Constructeur
    public Categorie() {
    }
    
    public Integer getId() {return this.id;}
    public void setId(Integer id) {this.id = id;}
    
    public String getLibelle() {return this.libelle;}
    public void setLibelle(String libelle) {this.libelle = libelle;}
    
    public String getDescription() {return this.description;}
    public void setDescription(String description) {this.description = description;}
    
    
    //Redéfinition des methodes
    public String toString(){
        return "Libellé : "+this.libelle+"\nDescription : "+this.description;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = hash * 9 + this.id.hashCode();
        hash = hash * 7 + this.libelle.hashCode();
        hash = hash * 12 + this.description.hashCode();
        return  hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categorie other = (Categorie) obj;
        if (!Objects.equals(this.libelle, other.libelle)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
}
