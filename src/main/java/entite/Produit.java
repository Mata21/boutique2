/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author toma
 */
public class Produit {
    private Long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;
    
    // Constructeurs
    public Produit() {

    }
    
    public Long getId() {return this.id;}
    public void setId(Long id) {this.id = id;}
    
    public String getLibelle() {return this.libelle;}
    public void setLibelle(String libelle) {this.libelle = libelle;}
    
    public double getPrixUnitaire() {return this.prixUnitaire;}
    public void setPrixUnitaire(double prixUnitaire) {this.prixUnitaire = prixUnitaire;}
    
    public LocalDate getDatePeremption() {return this.datePeremption;}
    public void setDatePeremption(LocalDate datePeremption) {this.datePeremption = datePeremption;}
    
    public Categorie getCategorie() { return this.categorie; }
    public void setCategorie(Categorie categorie) { this.categorie = categorie; }
    
    
    // Les méthodes de la classe
    public boolean estPerime() {
        return this.datePeremption.isBefore(LocalDate.now());
    }
    
    public boolean estPerime(LocalDate ref) {
                if (this.datePeremption == null || ref == null) {
            return false;
        }
        return this.datePeremption.isBefore(ref);
    }
    
    // Redéfintion des méthodes
    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 3 + this.id.hashCode();
        hash = (int) (hash * 9 + this.prixUnitaire);
        hash = hash * 5 + this.libelle.hashCode();
        hash = hash * 10 + this.datePeremption.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if (Double.doubleToLongBits(this.prixUnitaire) != Double.doubleToLongBits(other.prixUnitaire)) {
            return false;
        }
        if (!Objects.equals(this.libelle, other.libelle)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.datePeremption, other.datePeremption)) {
            return false;
        }
        if (!Objects.equals(this.categorie, other.categorie)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "\nLibelle : "+this.libelle+"\nPrix Unitaire : "+this.prixUnitaire+"\nCatégorie : "+this.categorie.getLibelle()+"\nDate de Péremption : "+this.datePeremption;
    }
    
}
