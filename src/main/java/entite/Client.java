/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.util.Objects;

/**
 *
 * @author toma
 */
public class Client extends Personne{
    private String carteVisa;
    private String cin;

    public Client() {

    }
    
    public String getCarteVisa() {
        return this.carteVisa;
    }
    
    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }
    
    public String getCin() {
        return this.cin;
    }
    
    public void setCin(String cin) {
        this.cin = cin;
    }
    
    // Redéfinition des methodes
    @Override
    public String toString() {
        return super.toString()+"\nCarte Visa : "+this.carteVisa;
    }
    
    @Override
    public int hashCode() {
        int hash = 1;
        hash = 21 * hash + super.hashCode();
        hash = hash * 3 + this.carteVisa.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.carteVisa, other.carteVisa)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
