/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.util.Objects;

/**
 *
 * @author toma
 */
public class ProduitAchete {
    private int quantite;
    private double remise;
    private Produit produit;
    private Achat achat;
    
    public ProduitAchete(){
        this.quantite = 1;
        this.remise = 0;
    }
    
    public Produit getProduit() { return this.produit;}
    public  void setProduit(Produit produit) { this.produit = produit;}
    
    public int getQuantite() { return this.quantite; }
    
    public double getRemise() {return this.remise;}
    
    public double getPrixTotal() {
        return this.produit.getPrixUnitaire() * this.quantite - this.remise;
    }
    
    // Redéfintion des methodes 
    public String toString() {
        return "Libelle : "+this.produit.getLibelle()+"\nQuantite : "+this.quantite+"\nRemise : "+this.getRemise()*100+" %"+"\nPrix Total : "+this.getPrixTotal();
    }
    
    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 12 + this.quantite;
        hash = (int)(hash * 17 + this.remise);
        hash = hash * 22 + this.produit.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.produit, other.produit)) {
            return false;
        }
        return true;
    }
    
    
    
}
