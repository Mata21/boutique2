/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;


/**
 *
 * @author toma
 */
public class Personne {
    protected Long id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    
    // Constructeur
    public Personne() {
       
    }
    
    
    
    // Les methodes
    public int getAge() {
        return getAge(LocalDate.now());
    }
    
    public Long getId() {
        return this.id;
    }
    
    public int getAge(LocalDate ref) {
        if(this.dateNaissance == null || ref == null) {
            throw new IllegalArgumentException("Le champs dateNaissance ou la sate de référence est null.");
        }
        Period period = Period.between(this.dateNaissance, ref);
        return period.getYears() < 0 ? - period.getYears() : period.getYears();
    }
    
    // Redéfinition des Mehodes
    @Override
    public String toString() {
        return "Nom : "+this.nom+"\nPrenom : "+this.prenom+"\nAge : "+this.getAge()+" ans";
    }
    
    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 18 + this.id.hashCode();
        hash = hash * 15 + this.nom.hashCode();
        hash = hash * 8 + this.prenom.hashCode();
        hash = hash * 10 + this.dateNaissance.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateNaissance, other.dateNaissance)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
