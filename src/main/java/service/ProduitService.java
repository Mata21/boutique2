/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entite.Produit;
import java.util.List;

/**
 *
 * @author toma
 */
public class ProduitService extends ParentService<Produit, Long>{

    @Override
    public Long getId(Produit t) {
        return t.getId();
    }
    
    public ProduitService() {
        super();
    }
    
    public ProduitService(List<Produit> liste) {
        super(liste);
    }
    
        
    
}
