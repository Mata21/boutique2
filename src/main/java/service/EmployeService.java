/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entite.Employee;
import java.util.List;

/**
 *
 * @author toma
 */
public class EmployeService extends ParentService<Employee, Long>{

    @Override
    public Long getId(Employee t) {
        return t.getId();
    }
    
    public EmployeService() {
        super();
    }
    
    public EmployeService(List<Employee> liste) {
        super(liste);
    }
    
   }
