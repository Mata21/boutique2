/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entite.Categorie;
import java.util.List;

/**
 *
 * @author toma
 */
public class CategorieService extends ParentService<Categorie, Integer>{
    
   
    @Override
    public Integer getId(Categorie t) {
        return t.getId();
    }

    public CategorieService() {
        super();
    }
    
    public CategorieService(List<Categorie> liste) {
        super(liste);
    }
    
    
    
}
