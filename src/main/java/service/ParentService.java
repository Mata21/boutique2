/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public abstract class  ParentService <T, ID> {
    
    protected List<T> liste;
    protected ID id;
    
    ParentService() {
        this.liste = new ArrayList<T>();
    }
    
    ParentService(List<T> liste) {
        this.liste = liste;
    }
    
    
    public abstract ID getId(T t);
    
    // Ajouter un Achat
    public void ajouter(T e) { 
        liste.add(e);
    }
    
    // Modifier un Achat
    public void modifier(T e) {
        for(T t : liste) {
            if(getId(t).equals(getId(e))){
                liste.set(liste.indexOf(t), e);  
                break;
            } 
        }    
    }
    
    //Trouver un achat
    public T trouver(ID id) {
        T find = null;
        for(T t : liste) {
            if(getId(t).equals(id)) {
                find = t;
                break;
            }
        }
        return find;
    }
    
    // Supprimer avec id
    public void supprimer(Long id) {
        for(T t : liste) {
            if(getId(t).equals(id)) {
                liste.remove(t);
                return;
            }
        }
    }
    
    // Supprimer avec la classe
    public void supprimer(T t) {
        liste.remove(t);
    }
    
    // Lister les Achats.
    public List<T> lister() {
        return liste;
    }
    
    //Lister à partir de début un nombre d'achat
    public List<T> lister(int debut, int nombre) {
        List<T> element = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++) {
            element.add(liste.get(i));
        }
        return element;
    }

    
}
