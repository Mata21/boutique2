/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entite.Client;
import java.util.List;

/**
 *
 * @author toma
 */
public class ClientService extends ParentService<Client, Long>{
    

    @Override
    public Long getId(Client t) {
       return t.getId();
    }
    
    public ClientService(List<Client> liste) {
        super(liste);
    }
      
}
