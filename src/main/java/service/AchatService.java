/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entite.Achat;
import java.util.List;



/**
 *
 * @author toma
 */
public class AchatService extends ParentService<Achat, Long>{

    @Override
    public Long getId(Achat t) {
        return t.getId();
    }
    
    public AchatService() {
        super();
    }
    
    public AchatService(List<Achat> liste) {
        super(liste);
    }
    
}
